﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BloodTraceWebApi.Helper;
using BloodTraceWebApi.Models;

namespace BloodTraceWebApi.Controllers
{
    [Authorize]
    public class BloodUsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public IHttpActionResult Post([FromBody] BloodUser bloodUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var stream = new MemoryStream(bloodUser.ImageArray);
            var guid = Guid.NewGuid().ToString();
            var file = String.Format("{0}.jpg", guid);
            var folder = "~/Content/Users";
            var fullPath = String.Format("{0}/{1}", folder, file);
            var response = FilesHelper.Uploadphoto(stream, folder, file);
            if (response)
            {
                bloodUser.ImagePath = fullPath;
            }


            var user = new BloodUser()
            {
                UserName = bloodUser.UserName,
                Email = bloodUser.Email,
                Phone = bloodUser.Phone,
                Country = bloodUser.Country,
                BloodGroup = bloodUser.BloodGroup,
                ImagePath = bloodUser.ImagePath,
                Date = bloodUser.Date
            };

            db.BloodUsers.Add(user);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.Created);
        }

        public IEnumerable<BloodUser> Get(string bloodGroup, string Country)
        {
            var bloodUsers = db.BloodUsers.Where(x => x.BloodGroup.StartsWith(bloodGroup) && x.Country.StartsWith(Country));
            return bloodUsers;
        }

        public IEnumerable<BloodUser> Get()
        {
            var latestUsers = db.BloodUsers.OrderByDescending(x => x.Date);
            return latestUsers;

        }
    }
}